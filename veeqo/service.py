# -*- encoding: utf-8 -*-
import logging
import requests

from django.conf import settings
from http import HTTPStatus

# from rich.pretty import pprint

from .models import VeeqoError


logger = logging.getLogger(__name__)


class Veeqo:
    def __init__(self):
        self.url = settings.VEEQO_URL
        self.key = settings.VEEQO_API_KEY

    def _headers(self):
        return {"accept": "application/json", "x-api-key": self.key}

    def customers(self):
        count = 0
        page = 1
        page_size = 30
        customers = []
        while True:
            response = requests.get(
                "{}/customers".format(self.url),
                params={"page": page, "page_size": page_size},
                headers=self._headers(),
            )
            if HTTPStatus.OK == response.status_code:
                data = response.json()
                count = count + len(data)
                # print()
                # print()
                # pprint(data, expand_all=True)
                customers = customers + data
                print(f"Page: {page}. Count: {count}. Len(data): {len(data)}.")
                page = page + 1
                if not data:
                    break
            else:
                breakpoint()
                logger.error(response)
                raise VeeqoError(
                    "Cannot get customers (page {}, page_size {}). "
                    "Status code {}".format(
                        page, page_size, response.status_code
                    )
                )
        return customers

    def orders(self):
        count = 0
        page = 1
        page_size = 30
        sales_orders = []
        while True:
            response = requests.get(
                "{}/orders".format(self.url),
                params={"page": page, "page_size": page_size},
                headers=self._headers(),
            )
            if HTTPStatus.OK == response.status_code:
                data = response.json()
                count = count + len(data)
                # pprint(data, expand_all=True)
                sales_orders = sales_orders + data
                print(f"Page: {page}. Count: {count}. Len(data): {len(data)}.")
                page = page + 1
                if not data:
                    break
            else:
                breakpoint()
                logger.error(response)
                raise VeeqoError(
                    "Cannot get sales orders (page {}, page_size {}). "
                    "Status code {}".format(
                        page, page_size, response.status_code
                    )
                )
        return sales_orders

    def products(self):
        page = 1
        page_size = 25
        result = []
        while True:
            response = requests.get(
                "{}/products".format(self.url),
                params={"page": page, "page_size": page_size},
                headers=self._headers(),
            )
            if HTTPStatus.OK == response.status_code:
                data = response.json()
                # if there is no data, then we have finished
                if not data:
                    break
                print()
                print("Page: {} ({} records)".format(page, len(result)))
                for count, row in enumerate(data, start=1):
                    sellables = row["sellables"]
                    for sellable in sellables:
                        sku = sellable["sku_code"]
                        if sku:
                            print(f"{count}. {sku}")
                            result.append(row)
                            break
                page = page + 1
            else:
                breakpoint()
                logger.error(response)
                raise VeeqoError(
                    "Cannot get products (page {}, page_size {}). "
                    "Status code {}".format(
                        page, page_size, response.status_code
                    )
                )
        return result
