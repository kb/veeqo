# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from veeqo.service import Veeqo

import json
import datetime


class Command(BaseCommand):

    help = "Veeqo - download sales orders #6188"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        veeqo = Veeqo()
        sales_orders = veeqo.orders()
        count = len(sales_orders)

        # Write sales orders to json file
        thisday = datetime.date.today()
        with open(f"{thisday}_sales-orders.json", "w") as fn:
            json.dump(sales_orders, fn, indent=2)

        self.stdout.write("{} - Complete ({} orders)".format(self.help, count))
