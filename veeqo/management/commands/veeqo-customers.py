# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from veeqo.service import Veeqo

import json
import datetime


class Command(BaseCommand):

    help = "Veeqo - download customers #6188"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        veeqo = Veeqo()
        customers = veeqo.customers()
        count = len(customers)

        # Write customers to json file
        thisday = datetime.date.today()
        with open(f"{thisday}_customers.json", "w") as fn:
            json.dump(customers, fn, indent=2)

        self.stdout.write(
            "{} - Complete ({} customers)".format(self.help, count)
        )
