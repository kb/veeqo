# -*- encoding: utf-8 -*-
import json

from django.core.management.base import BaseCommand
from django.utils import timezone

from rich.pretty import pprint
from veeqo.service import Veeqo


class Command(BaseCommand):

    help = "Veeqo - download products #6188"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        veeqo = Veeqo()
        products = veeqo.products()
        file_name = "veeqo-products-{}.json".format(
            timezone.now().strftime("%Y-%m-%d")
        )
        with open(file_name, "w", encoding="utf-8") as f:
            json.dump(products, f, indent=2)
        # pprint(products, expand_all=True)
        self.stdout.write(
            "{} - Complete ({} products)".format(self.help, len(products))
        )
