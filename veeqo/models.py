# -*- encoding: utf-8 -*-
from django.db import models


# logger = logging.getLogger(__name__)


class VeeqoError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))
