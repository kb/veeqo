Veeqo
*****

Django Veeqo
https://www.kbsoftware.co.uk/docs/app-veeqo.html

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-veeqo
  # or
  virtualenv --python=python3 venv-veeqo

  source venv-veeqo/bin/activate
  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
