# -*- encoding: utf-8 -*-
import os

from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = "Veeqo Demo Data"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        self.stdout.write("Complete: {}".format(self.help))
